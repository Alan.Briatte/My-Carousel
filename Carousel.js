var Marvel = [{
        src: 'img/marvel/antman.jpg',
        alt: 'Il est petit !'
            },
    {
        src: 'img/marvel/blackw.jpg',
        alt: 'Elle est belle !'
            },
    {
        src: 'img/marvel/captain.jpg',
        alt: 'Je l\'aime pas'
            },
    {
        src: 'img/marvel/hulk.jpg',
        alt: 'Il est solide !'
            },
    {
        src: 'img/marvel/ironman.jpg',
        alt: 'Il est classe !'
            },
    {
        src: 'img/marvel/spider.jpg',
        alt: 'Le BEST <3'
            },
    {
        src: 'img/marvel/thor.jpg',
        alt: 'Parce que je le vaux bien !'
            }
        ];

var Dc = [{
        src: 'img/dc/aqua.jpg',
        alt: 'Il est plus solide que hulk'
            },
    {
        src: 'img/dc/batman.jpg',
        alt: 'Il est plus fort que superman'
            },
    {
        src: 'img/dc/cyborg.jpg',
        alt: 'Il est plus en kit qu\'un meuble ikea'
            },
    {
        src: 'img/dc/flash.jpg',
        alt: 'Il est meilleur que tout le monde'
            },
    {
        src: 'img/dc/superman.jpg',
        alt: 'Il a son slip par dessus son pantalon :/'
            },
    {
        src: 'img/dc/wonder.jpg',
        alt: 'Ouai elle est plus belle que Black Widow'
            }
        ];

class Carousel {
    constructor(pictures, nbTumbnails, auto, div) {
        // tableau d'image
        this.pictures = pictures;
        // nombres de miniatures
        this.nbThumbnails = nbTumbnails;
        // index pour l'image principale
        this.currentPicture = 0;
        // index pour la gestion des miniatures
        this.thumbnailIndex = 0;
        // permet d'attendre que l'animation soit fini(flag)
        this.runningAnimation = false;
        // div utilisé pour le carousel
        this.div = div;
        // mode automatique
        this.auto = auto;
        // si le nombre de miniatures voulu est superieur au nombres d'images une erreur apparait
        if (this.nbThumbnails > this.pictures.length) {
            alert('Erreur dans la création du Carousel !!');
        } else {
            // sinon la création du carousel est lancé
            this.makeCarousel();
        }
    }

    // fonction de création complète du carousel
    makeCarousel() {
        // div qui va contenir l'intégralité de mon carousel
        this.carousel = document.createElement('div');
        this.carousel.setAttribute('class', 'carousel');
        document.getElementById(this.div).appendChild(this.carousel);

        // cadre pour l'image principale
        this.frame = document.createElement('div');
        this.frame.setAttribute('class', 'frame');
        this.carousel.appendChild(this.frame);

        // image principale
        this.mainPicture = document.createElement('div');
        this.mainPicture.setAttribute('class', 'mainPicture');
        this.frame.appendChild(this.mainPicture);

        // description
        this.description = document.createElement('div');
        this.description.setAttribute('class', 'description');
        this.description.innerHTML = this.pictures[this.currentPicture].alt;
        this.mainPicture.appendChild(this.description);

        // div de contrôle (bouton précédent et suivant)
        this.divControl = document.createElement('div');
        this.divControl.setAttribute('class', 'divControl');
        this.carousel.appendChild(this.divControl);

        // bouton précédent
        this.buttonPrevious = document.createElement('div');
        this.buttonPrevious.setAttribute('class', 'controls');
        this.buttonPrevious.style.backgroundImage = 'url(img/previous.svg)';
        this.buttonPrevious.onclick = () => {
            // va me permettre d'attendre que mon animation soit fini avant de pouvoir recliquer
            if (!this.runningAnimation) {
                this.currentPicture--;
                this.runningAnimation = true;
                this.previous();
                this.changePicture();
                setTimeout(() => {
                    this.runningAnimation = false;
                }, 500);
            }
        };
        this.divControl.appendChild(this.buttonPrevious);

        // création de ma div de miniatures
        this.divThumbnails = document.createElement('div');
        this.divThumbnails.setAttribute('class', 'divThumbnails');
        this.divControl.appendChild(this.divThumbnails);

        // création des miniatures en fonction du nombre de photo voulu
        for (let i = 0; i < this.nbThumbnails; i++) {
            let thumbnail = document.createElement('div');
            thumbnail.setAttribute('class', 'thumbnails');
            thumbnail.style.backgroundImage = 'url(' + this.pictures[i].src + ')';
            thumbnail.onclick = () => {
                this.chooseThumbnail(i);
            };
            this.divThumbnails.appendChild(thumbnail);
        }

        // création du bouton suivant
        this.buttonNext = document.createElement('button');
        this.buttonNext.setAttribute('class', 'controls nextButton');
        this.buttonNext.style.backgroundImage = 'url(img/next.svg)';
        this.buttonNext.onclick = () => {
            if (!this.runningAnimation) {
                this.currentPicture++;
                this.runningAnimation = true;
                this.next();
                this.changePicture();
                setTimeout(() => {
                    this.runningAnimation = false;
                }, 500);
            }
        };
        this.divControl.appendChild(this.buttonNext);
        // Affichage de la première image
        this.mainPicture.style.backgroundImage = 'url(' + this.pictures[this.currentPicture].src + ')';
        // application de la bordure
        this.setBorder();
        // automatisation du carousel
        if (this.auto) {
            this.autoRun = setInterval(() => {
                this.buttonNext.click();
            }, 3000);
        }
    }

    // la miniature choisi va devenir l'image principal
    chooseThumbnail(id) {
        // remplacement de l'index currentPicture par celui selectionné avec la miniature seulement si l'id renvoyé est différent de l'index this.currentPicture
        if (id != this.currentPicture) {
            this.currentPicture = id;
            this.setBorder();
            this.changePicture();
            // si l'option auto est active le timer doit être reset si je choisi une image puis remis en marche
            if (this.auto) {
                clearInterval(this.autoRun);
                this.autoRun = setInterval(() => {
                    this.buttonNext.click();
                }, 3000);
            }
        }
    }

    // change l'image principal
    changePicture() {
        // création de la prochaine image
        this.nextSlide = document.createElement('div');
        this.nextSlide.setAttribute('class', 'nextSlide');
        this.nextSlide.style.backgroundImage = 'url(' + this.pictures[this.currentPicture % this.pictures.length].src + ')';
        this.frame.appendChild(this.nextSlide);
        // création de la prochaine description
        this.description = document.createElement('div');
        this.description.setAttribute('class', 'description');
        this.description.innerHTML = this.pictures[this.currentPicture % this.pictures.length].alt;
        this.nextSlide.appendChild(this.description);
        // attente avant de supprimer l'ancienne image
        setTimeout(() => {
            this.carousel.firstChild.firstChild.remove();
        }, 1000);
    }

    // miniature précédente
    previous() {
        // si l'index this.currentPicture est inferieur à 0 on le force à être égal au dernier index de mon tableau d'image
        if (this.currentPicture < 0) {
            this.currentPicture = this.pictures.length - 1;
        }
        // ajout de la dernière miniature à la class disparition pour l'animer
        this.divThumbnails.lastChild.classList.add('disappear');
        // retour a la fin de mon tableau d'image
        if (this.thumbnailIndex == 0) {
            this.thumbnailIndex = this.pictures.length;
        }
        this.thumbnailIndex--;
        // création de la miniature précédente
        let prevThumbnail = document.createElement('div');
        prevThumbnail.style.backgroundImage = 'url(' + this.pictures[(this.thumbnailIndex) % this.pictures.length].src + ')';
        prevThumbnail.setAttribute('class', 'thumbnails');
        prevThumbnail.id = this.thumbnailIndex % this.pictures.length;
        prevThumbnail.onclick = () => {
            this.chooseThumbnail(prevThumbnail.id);
        }
        // append la miniature en première position
        this.divThumbnails.insertBefore(prevThumbnail, this.divThumbnails.firstChild);
        // ajout à la class appear pour l'animation
        prevThumbnail.classList.add('appear');
        setTimeout(() => {
            // suppression de la dernière miniature
            this.divThumbnails.lastChild.remove();
            // suppression de la class d'animation
            prevThumbnail.classList.remove('appear');
        }, 300);
        // mise en place de la bordure
        this.setBorder();
    }

    // miniature suivante
    next() {
        // si l'index this.currentPicture est superieur à la longueur de mon tableau il redémarre 
        if (this.currentPicture > this.pictures.length - 1) {
            this.currentPicture = 0;
        }
        // ajout de la première miniature à la class d'animation
        this.divThumbnails.firstChild.classList.add('disappear');
        // incrément de thumbnailIndex
        this.thumbnailIndex++;
        // création de la prochaine miniature
        let nextThumbnail = document.createElement('div');
        nextThumbnail.style.backgroundImage = 'url(' + this.pictures[((this.divThumbnails.children.length - 1) + this.thumbnailIndex) % this.pictures.length].src + ')';
        // permet de récupérer l'index de ma miniature
        let id = ((this.divThumbnails.children.length - 1) + this.thumbnailIndex) % this.pictures.length;
        nextThumbnail.setAttribute('class', 'thumbnails');
        nextThumbnail.onclick = () => {
            this.chooseThumbnail(id);
        }
        // ajout à la class d'animation 
        nextThumbnail.classList.add('appear');
        this.divThumbnails.appendChild(nextThumbnail);
        // supprime la première miniature en laissant le temps à l'animation
        setTimeout(() => {
            this.divThumbnails.firstChild.remove();
            nextThumbnail.classList.remove('appear');
        }, 300);
        this.setBorder();
    }

    // ajout de la bordure sur la miniature séléctionné
    setBorder() {
        for (let i = 0; i < this.divThumbnails.children.length; i++) {
            this.divThumbnails.children[i].classList.remove('highlight');
        }
        for (let i = 0; i < this.divThumbnails.children.length; ++i) {
            let src = 'url("' + this.pictures[this.currentPicture].src + '")';
            if (src === this.divThumbnails.children[i].style.backgroundImage)
                this.divThumbnails.children[i].classList.add('highlight');
        }
    }
}

// new carousel(tableau d'images voulu, le nombres de miniatures à afficher, mode auto, div pour placer le carousel (ne marche qu'avec des id pour le moment))

var alan = new Carousel(Marvel, 5, 1, "zone");
var toutoune = new Carousel(Dc, 3, 0, "zone2");
var gwen = new Carousel(Marvel, 4, 0, "zone3");
